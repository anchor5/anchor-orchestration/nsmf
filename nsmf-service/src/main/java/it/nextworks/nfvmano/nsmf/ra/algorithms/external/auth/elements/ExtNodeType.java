package it.nextworks.nfvmano.nsmf.ra.algorithms.external.auth.elements;

public enum ExtNodeType {
    REGULAR,
    gNB,
    SC,
    BS
}
