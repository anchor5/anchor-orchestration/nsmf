# Network Slice Management Function 


## Installation
- Create a folder in you workspace, this will be your root folder for the project. (ie ANCHOR/) 
- Checkout in the root folder this repository and the orchestrator-common-lib repository.
- make sure you have installed java 8 and maven 3.X 
 *(tested with openjdk version 1.8.0_312 and Apache Maven 3.6.3)*
- in the root folder run this command:

> mvn clean install -f orchestrator-common-libs/pom.xml

> mvn clean install -f nsmf/pom.xml

- the final jar will be in 'nsmf/nsmf-app/target/' folder

